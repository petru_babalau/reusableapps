from django.db import models


class Language(models.Model):
    name = models.CharField(max_length=100)
    iso_639_1_code = models.CharField(max_length=2, null=True, blank=True, default=None)
    iso_639_2_code = models.CharField(max_length=3, null=True, blank=True, default=None)
    script_language_id = models.CharField(max_length=20, null=True, blank=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
