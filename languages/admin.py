from django.contrib import admin
from .models import Language


class LanguageAdmin(admin.ModelAdmin):
    search_fields = ('name', )


admin.site.register(Language, LanguageAdmin)
